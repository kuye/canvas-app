window.onload = function (){
  var canvas = document.getElementById("canvas");
  canvas.width = 800;
  canvas.height = 600;

  var context = canvas.getContext("2d");
  context.moveTo(100,100);
  context.lineTo(200,100);
  context.lineWidth = 5;
  context.strokeStyle = "#AA394C";
  context.stroke();

  console.log("context 加载完毕!");
}
